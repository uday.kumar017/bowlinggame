Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do 
  	resources :games, only: [:create,:show, :destroy], :defaults => { :format => 'json' } do
  		post :new_ball
  	end
  end

end
