
# README

#Please use Ruby 2.4.1 & Rails 5.1.7

Following are the APIs:

1.  POST: http://localhost:3000/api/games #To start a new bowling game

2.  POST: http://localhost:3000/api/games/ZsiYWDFPzxSdyLgufcBLoA/new_ball?pins=4 #Input the number of pins knocked down by each ball

3.  GET: http://localhost:3000/api/games/ZsiYWDFPzxSdyLgufcBLoA #score for each frame and total score
