class Game < ApplicationRecord
  has_many :frames, dependent: :destroy
  has_many :balls, through: :frames

  COMPLETED = true

 
  def update_score(frame,score)
  	frame.update(score: score)
  end	

  def update_strike_spares(score, update_strike = false)
  	frames = self.frames
  	strike_ids = Ball.where(frame_id: frames.pluck(:id),value: ["strike","spare"]).pluck(:frame_id)
	strike_frames = frames.select{|a| a if a.score == nil && strike_ids.include?(a.id)}
	if strike_frames.present?
	  	last_frame = frames.last
	  	s_last_frame_balls = strike_frames.last.balls
	  	last_frame_balls = last_frame.balls
	  	strike_frames_length = strike_frames.length
	  	strike_frames.each_with_index do |sf,i|	
	  		# ---------------
	  		values = sf.balls.map(&:value)
			sf_index = frames.index(sf)
	  		if values.include?('strike')
	  			next_frames = [frames[sf_index+1], frames[sf_index+2]].flatten.compact
	  			val_sum = next_frames.present? ? 0 : nil
	  			next_frames.each_with_index{|a, i| 
	  				p_balls = a.balls
	  				if i == 0
		  				val_sum += p_balls.map(&:pins).sum
	  				else 
		  				val_sum += p_balls.first.pins
		  			end
		  			val_sum = nil if next_frames.count == 1 && p_balls.count == 1
	  				break if p_balls.count == 2
	  			}
	  			score += 10 + val_sum.to_i if val_sum.present?
	  		elsif values.include?('spare')
	  			next_frames = frames[sf_index+1]
				val_sum = (next_frames.present? ? next_frames.balls.first.pins : nil)
	  			score += 10 + val_sum.to_i if val_sum.present?
	  		end
	  		# ------------------
			sf.update(score: score) if val_sum.present?
		end
  	end
  	score
  end

end
