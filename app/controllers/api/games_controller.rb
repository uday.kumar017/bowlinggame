class Api::GamesController < ApplicationController
	before_action :set_game, only: [:new_ball]

  def create
  	game = Game.create(code: SecureRandom.urlsafe_base64(16))
    render json: game, status: :success
  end

  def show
  	@game = Game.find_by(code: params[:id])
    res = get_frame_balls(@game)
    render json: res, status: :success
  end

  def new_ball
  	frames = @game.frames
  	if params[:pins].present? && params[:pins].to_i <= 10
	    unless frames.present?
	    	frame = @game.frames.create
	    	ball = frame.balls.create(pins: params[:pins],value: params[:pins] == "10" ? "strike" : params[:pins])
	    else
	    	total_score = frames.where.not(score: nil).last.try(:score)
	    	last_frame = frames.last
	    	second_last_frame = frames.last(2).first
	    	is_prev_strike = frames.last.score == nil
	    	balls_val = last_frame.balls.pluck(:value)
	    	
		    if frames.count == 10
		    	val = params[:pins] == "10"  ? "strike" : params[:pins]
		    	val = last_frame.balls.pluck(:pins).sum + params[:pins].to_i == 10 ? "spare" : val if balls_val.length == 1 && val.to_s != 'strike'
		    	is_status_updated = false
				ball = last_frame.balls.create(pins: params[:pins],value: val)
	    		if last_frame.balls.length == 3 || (!["strike","spare"].include?(val) && last_frame.balls.length == 2)
	    			total_score = total_score.to_i + last_frame.balls.pluck(:pins).sum.to_i
	    			total_score = last_frame.update(score: total_score)
					is_status_updated = @game.update(status: Game::COMPLETED) 
				else
				end
				if is_status_updated
					res = get_frame_balls(@game)
					render json: res, status: :created
					return false 
				end

	    	elsif  balls_val.length == 2
	    		frame = @game.frames.create
	    		ball = frame.balls.create(pins: params[:pins],value: params[:pins] == "10" ? "strike" : params[:pins])
	    		total_score = @game.update_strike_spares(total_score.to_i) if balls_val.include?('spare')
	    	elsif balls_val.length == 1 && balls_val.include?("strike")
	    		strike_frames = frames.select{|a| a if a.score == nil}
	    		frame = @game.frames.create
	    		ball = frame.balls.create(pins: params[:pins],value: params[:pins] == "10" ? "strike" : params[:pins])
	    		total_score = @game.update_strike_spares(total_score.to_i, true) if is_prev_strike && strike_frames.length >= 2 
	    	elsif balls_val.length == 1 && !balls_val.include?("strike")
	    		if (last_frame.balls.pluck(:pins).sum + params[:pins].to_i) > 10
		    		render json: { errors: "Please enter valid Pins" }, status: :unprocessable_entity 
		    		return false
		    	end
	    		val = balls_val[0].to_i + params[:pins].to_i  
	    		ball = last_frame.balls.create(pins: params[:pins],value: val == 10  ? "spare" : params[:pins])
	    		total_score = @game.update_strike_spares(total_score.to_i) if is_prev_strike && ball.value != 'spare'
	    		@game.update_score(second_last_frame,total_score = total_score.to_i + second_last_frame.balls.pluck(:pins).sum) if second_last_frame.score.blank? && ball.value != 'spare'
	    		@game.update_score(last_frame,total_score.to_i + last_frame.balls.pluck(:pins).sum) if val != 10 && second_last_frame.present? && frames.length > 1 
	    	end
	    end
	    res = get_frame_balls(@game)
	    render json: res, status: :created
	else
		render json: { errors: "Please enter valid Pins" }, status: :unprocessable_entity
	end
  end


  def get_frame_balls(game)
  	res = {status: (game.status ? 'Game is Completed' : 'In-progress'), ball_id: game.code,total_score: 0,frames: []}
  	frames = game.frames
  	balls =  Ball.where(frame_id: frames.pluck(:id)).group_by(&:frame_id)
  	frames = frames.pluck(:id,:score).to_h
  	balls.each do |frame_id,v|
  		hash = {score: frames[frame_id],balls: []}
  		v.each do |ball|
  			hash[:balls] << {pins: ball.pins,value: ball.value}
  		end
  		res[:frames] << hash
  		res[:total_score] = frames[frame_id] if frames[frame_id].present?
  	end
  	res
  end

private
  def set_game
    @game = Game.find_by(code: params[:game_id])
    return render_failure_response("Invalid Code") if params[:game_id].blank? || @game.blank?
    return render_failure_response("Game is Completed") if @game.status == Game::COMPLETED
  end

  def render_failure_response(message)
    render :json => {:errors => message}, :status => :unprocessable_entity
    return false
  end
end
