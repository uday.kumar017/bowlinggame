class CreateBalls < ActiveRecord::Migration[5.1]
  def change
    create_table :balls do |t|
      t.references :frame, foreign_key: true
      t.integer :pins

      t.timestamps
    end
  end
end
