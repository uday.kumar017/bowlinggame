class CreateFrames < ActiveRecord::Migration[5.1]
  def change
    create_table :frames do |t|
      t.references :game, foreign_key: true
      t.integer :score
      t.timestamps
    end
  end
end
