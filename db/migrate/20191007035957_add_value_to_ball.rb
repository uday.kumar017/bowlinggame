class AddValueToBall < ActiveRecord::Migration[5.1]
  def change
    add_column :balls, :value, :string
  end
end
